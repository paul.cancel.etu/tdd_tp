package additionator;

public class Additionator {
    private static final String NULL_VALUE = "0.0";
    private static final String REGEX = "[//XYZ\n,]";

    public static String additionator(String str){
        double res = 0.0;
        int len = str.length();

        if(len != 0){
            String[] nb = str.split(REGEX);

            if(REGEX.contains(""+str.charAt(len-1))) return "Nombre attendu en fin de chaine";

            char tmp = str.charAt(0);
            boolean firstNumber = Character.isDigit(tmp);

            for(int i = 1; i<len; i++){
                char tmp2 = str.charAt(i);
                if(REGEX.contains(""+tmp) && REGEX.contains(""+str.charAt(i)) && firstNumber) {
                    String charRes;
                    if(tmp2 == '\n') charRes = "\\n";
                    else charRes = ""+tmp2;

                    return "Nombre attendu à la place de '"+ charRes + "' à la position " + (i+1);
                }

                if(!firstNumber) firstNumber = Character.isDigit(tmp2);
                tmp = tmp2;
            }

            for(String s: nb){
                res += Double.parseDouble(s);
            }
        }

        return Double.toString(res);
    }
}
