package foobarqix;

public class Foobarqix {
    private static final String THREE_VALUE = "Foo";
    private static final String FIVE_VALUE = "Bar";
    private static final String SEVEN_VALUE = "Qix";

    public static String fooBarQix(int n){
        StringBuilder sb = new StringBuilder();

        forThree(n, sb);
        forFive(n, sb);
        forSeven(n, sb);
        String nb = ""+n;
        searchNumbers(sb, nb);
        if(sb.isEmpty()){
            sb.append(nb);
        }
        return sb.toString();
    }

    private static void searchNumbers(StringBuilder sb, String nb) {
        int len = nb.length();
        for(int i = 0; i<len; i++){
            switch ((nb.charAt(i))){
                case '3': sb.append(THREE_VALUE);
                break;
                case '5': sb.append(FIVE_VALUE);
                break;
                case '7': sb.append(SEVEN_VALUE);
                break;
            }
        }
    }

    private static void forSeven(int n, StringBuilder sb) {
        if(n%7 == 0){
            sb.append(SEVEN_VALUE);
        }
    }

    private static void forFive(int n, StringBuilder sb) {
        if(n%5 == 0){
            sb.append(FIVE_VALUE);
        }
    }

    private static void forThree(int n, StringBuilder sb) {
        if(n%3 == 0){
            sb.append(THREE_VALUE);
        }
    }
}
