package foobarqix;

import static org.junit.Assert.assertEquals;

import org.junit.Test;



public class FoobarqixTest {
    int n1, n2, n3;
    
    @Test
    public void quelconqueTest(){
        n1 = 11;
        n2 = 1;
        n3 = 2;

        assertEquals(""+n1, Foobarqix.fooBarQix(n1));
        assertEquals(""+n2, Foobarqix.fooBarQix(n2));
        assertEquals(""+n3, Foobarqix.fooBarQix(n3));
    }

    @Test
    public void divisibleTest(){
        n1 = 9;
        n2 = 10;
        n3 = 14;

        assertEquals("Foo", Foobarqix.fooBarQix(n1));
        assertEquals("Bar", Foobarqix.fooBarQix(n2));
        assertEquals("Qix", Foobarqix.fooBarQix(n3));
    }

    @Test
    public void divisibleMultipleTest(){
        n1 = 60;
        n2 = 21;
        n3 = 140;

        assertEquals("FooBar", Foobarqix.fooBarQix(n1));
        assertEquals("FooQix", Foobarqix.fooBarQix(n2));
        assertEquals("BarQix", Foobarqix.fooBarQix(n3));
    }

    @Test
    public void containsTest(){
        n1 = 13;
        n2 = 52;
        n3 = 17;

        assertEquals("Foo", Foobarqix.fooBarQix(n1));
        assertEquals("Bar", Foobarqix.fooBarQix(n2));
        assertEquals("Qix", Foobarqix.fooBarQix(n3));
    }

    @Test
    public void contiensEtDiviseTest(){
        n1 = 15;
        n2 = 70;
        n3 = 105;

        assertEquals("FooBarBar", Foobarqix.fooBarQix(n1));
        assertEquals("BarQixQix", Foobarqix.fooBarQix(n2));
        assertEquals("FooBarQixBar", Foobarqix.fooBarQix(n3));
    }
}
