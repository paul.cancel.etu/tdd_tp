package additionator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AdditionatorTest {
    String str;

    @Test
    public void emptyTest(){
        str = "";

        assertEquals("0.0", Additionator.additionator(str));
    }

    @Test
    public void additionTest(){
        str = "1.0,2.0";

        assertEquals("3.0", Additionator.additionator(str));
    }

    @Test
    public void additionReelTest(){
        str = "1.5,2.3";

        assertEquals("3.8", Additionator.additionator(str));
    }

    @Test
    public void virguleErreurTest(){
        str = "1.0,3.0,";

        assertEquals("Nombre attendu en fin de chaine", Additionator.additionator(str));
    }

    @Test
    public void retourChariotTest(){
        str = "1.0\n3.0,2.0";

        assertEquals("6.0", Additionator.additionator(str));
    }

    @Test
    public void doubleSeparateurTest(){
        str = "27.0,\n55.0";

        assertEquals("Nombre attendu à la place de '\\n' à la position 6", Additionator.additionator(str));
    }

     @Test
     public void plusDeSeparateur(){
        str = "//|\n15.0|2.0|3.7";
        assertEquals("20.7", Additionator.additionator(str));

        str = "//blah\n2.0blah3.0";
        assertEquals("5.0", Additionator.additionator(str));
    
        str = "//|\n1.0|2.0,3.0";
        assertEquals("'|' attendu à la place de ',' à la position 8", Additionator.additionator(str));
    }
}
